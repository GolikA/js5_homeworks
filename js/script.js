function myFunc (e)
{
    let evt = window.event || e,
        obj = evt.srcElement || evt.target;
    if (obj.tagName == 'TABLE') return;
    while (obj.tagName != 'TD') obj = obj.parentNode;
    obj.className = obj.className ? '' : 'act';
}

onload = function ()
{
    let trg = document.getElementById ('myTBL');
    if (document.addEventListener) trg.addEventListener ('click', myFunc);
    else if (document.attachEvent) trg.attachEvent ('onclick', myFunc);
    else trg.onclick = myFunc;
};


